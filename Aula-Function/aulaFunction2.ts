namespace aulaFunction2
{
    function multiplicaPor(n:number): (x:number) => number {
        return function (x:number): number{
            return x * n;
        }
    }
    let duplicar = multiplicaPor(2);
    let resultado = duplicar(5);
    console.log(resultado);
    let triplicar = multiplicaPor(3);
    resultado = triplicar(5);
    console.log(resultado);
    
}