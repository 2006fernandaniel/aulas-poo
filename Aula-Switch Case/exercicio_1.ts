//Comentar
/*Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido
*/

namespace exercicio_1 
{
    let nivel: number;
    nivel = 10;

    switch(nivel)
    { 
        case 10: console.log("Avançado!!");
                break;
        case 9: console.log("Avançado!!");
                break;
        case 8: console.log("Bom!");
                break;
        case 7: console.log("Mediano!");
                break;
        case 6: console.log("Mediano!");
                break;
        case 5: console.log("Mediano!");
                break;
        case 4: console.log("Mais ou menos");
                break;
        case 3: console.log("Mais ou menos");
                break;
        case 2: console.log("Iniciante");
                break;
        case 1: console.log("Iniciante");
                break;
        default: console.log("Não é uma opção válida");

    }

}