//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.

namespace exercicio_3{
       //let livro: any[] = [{titulo:"Amor e Gelato", autor:"Jenna Evans Welch"}, 
        //{titulo:"Píppi a bordo", autor:"Astrid Lind Gren"}, 
        //{titulo:"As dezvantagens de morrer depois de você", autor:"Fernanda de Castro Lima"}, 
        //{titulo:"Amor e Sorte", autor:"Jenna Evans Welch"}];

        //let resultado = livros.map(function (res){
            //return res.titulo;
        //})
        //console.log(resultado);

        //let titulos =  livro.map((livro) => {
            //return livro.titulo

        //});
        //console.log(titulos);


/*Dado um array de objetos livros, contento os campos titulo e autor, crie um programa em 
Typescript que utilize a função filter() para encontrar todos os livros do autor com valor "Autor 3".
Em seguida, utilize a função map() para mostrar apenas os titulos dos livros encontrados.
O resultado deve ser exibido no console.*/

let livro: any[] = [
    {titulo:"Titulo 1", autor:"Autor 1"}, //0
    {titulo:"Titulo 2", autor:"Autor 2"}, //1
    {titulo:"Titulo 3", autor:"Autor 3"}, //2
    {titulo:"Titulo 4", autor:"Autor 4"}  //3
]
let autor3 = livro.filter((livro) => {
    return livro.autor === "Autor 3";
});

    console.log(autor3); 

    let titulos = autor3.map((livro) => {
            return livro.titulo;
})
        console.log(titulos);
}