//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.

namespace exercicio_2{
    let frutas: string[] = ["Laranja", "Uva", "Morando"];
    let i:number = 0;
    //console.log(frutas[0]); // 'Laranja'
    //console.log(frutas[1]); // 'Uva'
    //console.log(frutas[2]); // 'Morango'
    while (i < frutas.length){
        console.log(frutas[i]);
        i++;
    }


}
