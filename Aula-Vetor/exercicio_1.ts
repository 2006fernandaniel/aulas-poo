//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.

namespace exercicio_1{     // 0  1  2  3  4
    let numeros: number [] = [5, 4, 7, 8, 9];
    let soma: number = 0;

    for (let i = 0; i < numeros.length ; i++){
        soma = soma + numeros[i];
        //soma += numeros[i];
    }


    let result:number;
    result = soma;
    console.log(`A soma é: ${result}`); 

    //criando uma iteração com multiplicação
    let multi: number = 1;
    for(let i = 0; i < numeros.length ; i++){
        multi *= numeros[i];
    }
        console.log(`O resultado da multiplicação é: ${multi}`);
    
}