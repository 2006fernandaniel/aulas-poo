//Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno com as seguintes propriedades: "aluno"(string) "idade"(number) "notas"(array de número). Preencha o vetor com informações ficticías.
//Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente com o nome e a idade do aluno.

    interface Aluno{
        nome:string;
        idade:number;
        notas:number[];
    }
namespace exercicio_6{
    const alunos: Aluno[] = [
        {nome:"Aluno 1", idade: 20, notas: [10,10,10]},
        {nome:"Aluno 2", idade: 40, notas: [4,3,1]},
        {nome:"Aluno 3", idade: 60, notas: [2,1,8]},
        {nome:"Aluno 4", idade: 30, notas: [3,5,9]},

    ]

    alunos.forEach((aluno) =>{
        //console.log("-------------------------------------------------------")
        //console.log((aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / 3);
        let media = aluno.notas.reduce(
            (total, nota) => {return total + nota}
        )/aluno.notas.length
        if (media >= 7){
            console.log(`A média do aluno ${aluno.nome} é igual ${media} e está aprovado`);
        }
        else {
            console.log(`A média do aluno ${aluno.nome} é igual ${media} e está reprovado`);
        }
    })

}