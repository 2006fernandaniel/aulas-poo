//Crie um array com 6 números. Em seguida, use o método filter() para criar um novo array contendo apenas os números ímpares.

namespace exercicio_4{
    let numeros: number [] = [1, 2, 3, 4, 5, 6];

    let impares = numeros.filter(function(num) {
    return num % 2 === 1;
});

console.log(impares); //1, 3, 5
}